from django.db import models


class Post(models.Model):
    image = models.ImageField(upload_to='posts/images/')
    url = models.URLField(blank=True)
    name = models.CharField(max_length=100, null=True)
    title = models.CharField(max_length=100)
    content = models.TextField()

    def __str__(self):
        return self.name

class Main(models.Model):
    logo = models.ImageField(upload_to='main/', null=True)
    url = models.URLField(blank=True)
    sname = models.CharField(max_length=200, null=True)
    firsttext = models.CharField(max_length=200, null=True)
    telegram = models.URLField(blank=True)
    instagram = models.URLField(blank=True)
    address = models.CharField(max_length=200)
    email = models.CharField(max_length=100)
    phonenumber = models.CharField(max_length=20)
    about_title = models.CharField(max_length=50, null=True)
    about = models.TextField(null=True)

    def __str__(self):
        return self.sname

