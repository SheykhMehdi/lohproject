# Generated by Django 3.1.2 on 2020-10-23 14:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Main',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logo', models.ImageField(null=True, upload_to='main/')),
                ('url', models.URLField(blank=True)),
                ('sname', models.CharField(max_length=200, null=True)),
                ('firsttext', models.CharField(max_length=200, null=True)),
                ('telegram', models.URLField(blank=True)),
                ('instagram', models.URLField(blank=True)),
                ('address', models.CharField(max_length=200)),
                ('email', models.CharField(max_length=100)),
                ('phonenumber', models.CharField(max_length=20)),
                ('about_title', models.CharField(max_length=50, null=True)),
                ('about', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='posts/images/')),
                ('url', models.URLField(blank=True)),
                ('name', models.CharField(max_length=100, null=True)),
                ('title', models.CharField(max_length=100)),
                ('content', models.TextField()),
            ],
        ),
    ]
