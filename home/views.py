from django.shortcuts import render, get_object_or_404
from .models import Post, Main


def home(request):
    posts = Post.objects.all().order_by('-id')[:3]
    mains = Main.objects.get(id=1)
    return render(request, 'home.html', {'posts':posts, 'mains':mains})

def postdetail(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    mains = Main.objects.get(id=1)
    return render(request, 'postdetail.html', {'post':post, 'mains':mains})

def posts(request):
    posts = Post.objects.all().order_by('-id')
    mains = Main.objects.get(id=1)
    return render(request, 'posts.html', {'posts':posts, 'mains':mains})
