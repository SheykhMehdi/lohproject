from django.urls import path, include
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home, name="home"),
    path('posts/', views.posts, name="posts"),
    path('<int:post_pk>', views.postdetail, name="postdetail"),
]
